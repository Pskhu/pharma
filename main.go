package main

import (
	core "gitlab.ecopsy.com/linkis/go-linkis-core"
	entities "gitlab.ecopsy.com/linkis/go-linkis-entities"
	"strconv"
)

func main() {

	companiesSlice := []string{
		"Гедеон Рихтер",
		"Инвар",
		"Отисифарм",
		"Фармасинтез-Норд",
		"Alium",
		"Astellas",
		"Astra Zeneca",
		"Bausch Health",
		"Bayer",
		"Берлин-Хеми/А. Менарини",
		"Boehringer Ingelheim",
		"Chiesi",
		"Dr.Reddy's",
		"GSK",
		"Johnson and Johnson",
		"KRKA",
		"Lilly",
		"MSD",
		"Novartis",
		"Orion Pharma",
		"Pfizer",
		"Фармстандарт",
		"Roche",
		"Sandoz",
		"Sanofi",
		"Servier",
		"Stada",
		"Takeda",
		"Teva",
		"Р-Фарм",
		"Фармимэкс",
		"Биотэк",
		"ПРОМОМЕД",
		"Микроген",
		"Sun Pharma",
		"Биокад",
		"Акрихин",
		"LEO Pharma",
		"Solgar Vitamin and Herb",
		"Unipharm",
		"Валента Фармацевтика",
		"Вертекс",
		"Герофарм",
		"Ниармедик плюс",
		"ПИК-фарма",
		"Besins Healthcare",
		"Egis",
		"Glenmark",
		"Mirpharm",
		"Nativa",
		"Novo Nordisk",
		"Renewal (ПФК Обновление)",
		"Solopharm (ООО Гротекс)",
		"Gilead",
		"Abbott",
		"Abbvie",
	}

	core.BlockFunc = func(state *core.State) {

		var instructions core.Block

		state.Block("instruction", &instructions).Before(func(block core.BeforeBlock) {

		}).Main(func(b core.MainBlock) {
			b.Screen().AddHeader("Добрый день!")
			b.Screen().AddString("Мы рады, что вы участвуете в нашем опросе. Мы исследуем, какие ожидания у соискателей от работодателей, что ценят в своих компаниях сотрудники, какие компании воспринимаются участниками фармацевтического рынка как привлекательное место работы и почему.<br/>Вам будет предложено выбрать несколько компаний и поделиться своим мнением о них.<br/>Опрос займет около 10 минут.<br/>Если при заполнении опроса вы столкнулись с техническими сложностями, напишите по адресу: survey@ecopsy.ru.<br/><b>Важно: при заполнении опросника и переходе на следующий экран Вы не сможете вернуться назад и исправить ответы.</b>")
			b.Screen().SetSubmitText("Начать опрос")
		})

		var passport core.Block
		state.Block("passport", &passport).Main(func(block core.MainBlock) {
			block.Screen().AddHeader("Расскажите немного о себе")
			q := entities.Question{
				ID:             4,
				Code:           "gender",
				Type:           "radio",
				SubType:        "",
				Shuffle:        false,
				Bank:           "",
				Hardness:       0,
				TimeLimit:      0,
				Text:           "Ваш пол",
				Media:          "",
				Variants:       []entities.Variant{},
				AcceptEmpty:    false,
				MinVal:         0,
				MaxVal:         0,
				Step:           0,
				Placeholder:    "",
				DefaultValue:   "",
				ValidateStr:    "",
				SelectImage:    false,
				Multiple:       false,
				MultipleMin:    0,
				MultipleMax:    0,
				LikertVariants: nil,
				LMin:           0,
				LMax:           0,
				RMin:           0,
				RMax:           0,
				LText:          "",
				RText:          "",
				MText:          "",
				Lists:          nil,
				Trigger:        "",
				Target:         "",
				Show:           0,
				AttributeJson:  "",
			}
			q.AddVariant("1", "Мужской", "1")
			q.AddVariant("2", "Женский", "2")
			block.Screen().AddQuestion(q)

			q = entities.Question{
				ID:             5,
				Code:           "age",
				Type:           "radio",
				SubType:        "",
				Shuffle:        false,
				Bank:           "",
				Hardness:       0,
				TimeLimit:      0,
				Text:           "Ваш возраст ",
				Media:          "",
				Variants:       []entities.Variant{},
				AcceptEmpty:    false,
				MinVal:         0,
				MaxVal:         0,
				Step:           0,
				Placeholder:    "",
				DefaultValue:   "",
				ValidateStr:    "",
				SelectImage:    false,
				Multiple:       false,
				MultipleMin:    0,
				MultipleMax:    0,
				LikertVariants: nil,
				LMin:           0,
				LMax:           0,
				RMin:           0,
				RMax:           0,
				LText:          "",
				RText:          "",
				MText:          "",
				Lists:          nil,
				Trigger:        "",
				Target:         "",
				Show:           0,
				AttributeJson:  "",
			}
			q.AddVariant("1", "18-25", "1")
			q.AddVariant("2", "26-30", "2")
			q.AddVariant("3", "31-35", "3")
			q.AddVariant("4", "36-40", "4")
			q.AddVariant("5", "41-45", "5")
			q.AddVariant("6", "46-50", "6")
			q.AddVariant("7", "51-55", "7")
			q.AddVariant("8", "56 и более", "8")
			block.Screen().AddQuestion(q)

			q = entities.Question{
				ID:             6,
				Code:           "family",
				Type:           "radio",
				SubType:        "",
				Shuffle:        false,
				Bank:           "",
				Hardness:       0,
				TimeLimit:      0,
				Text:           "Ваше семейное положение",
				Media:          "",
				Variants:       []entities.Variant{},
				AcceptEmpty:    false,
				MinVal:         0,
				MaxVal:         0,
				Step:           0,
				Placeholder:    "",
				DefaultValue:   "",
				ValidateStr:    "",
				SelectImage:    false,
				Multiple:       false,
				MultipleMin:    0,
				MultipleMax:    0,
				LikertVariants: nil,
				LMin:           0,
				LMax:           0,
				RMin:           0,
				RMax:           0,
				LText:          "",
				RText:          "",
				MText:          "",
				Lists:          nil,
				Trigger:        "",
				Target:         "",
				Show:           0,
				AttributeJson:  "",
			}
			q.AddVariant("1", "Состою в браке", "1")
			q.AddVariant("2", "Не состою в браке", "2")

			block.Screen().AddQuestion(q)

			q = entities.Question{
				ID:             7,
				Code:           "family_count",
				Type:           "radio",
				SubType:        "",
				Shuffle:        false,
				Bank:           "",
				Hardness:       0,
				TimeLimit:      0,
				Text:           "Наличие детей",
				Media:          "",
				Variants:       []entities.Variant{},
				AcceptEmpty:    false,
				MinVal:         0,
				MaxVal:         0,
				Step:           0,
				Placeholder:    "",
				DefaultValue:   "",
				ValidateStr:    "",
				SelectImage:    false,
				Multiple:       false,
				MultipleMin:    0,
				MultipleMax:    0,
				LikertVariants: nil,
				LMin:           0,
				LMax:           0,
				RMin:           0,
				RMax:           0,
				LText:          "",
				RText:          "",
				MText:          "",
				Lists:          nil,
				Trigger:        "",
				Target:         "",
				Show:           0,
				AttributeJson:  "",
			}
			q.AddVariant("1", "Да", "1")
			q.AddVariant("2", "Нет", "2")
			block.Screen().AddQuestion(q)

			q = entities.Question{
				ID:             8,
				Code:           "farm_education",
				Type:           "radio",
				SubType:        "",
				Shuffle:        false,
				Bank:           "",
				Hardness:       0,
				TimeLimit:      0,
				Text:           "Уровень Вашего образования",
				Media:          "",
				Variants:       []entities.Variant{},
				AcceptEmpty:    false,
				MinVal:         0,
				MaxVal:         0,
				Step:           0,
				Placeholder:    "",
				DefaultValue:   "",
				ValidateStr:    "",
				SelectImage:    false,
				Multiple:       false,
				MultipleMin:    0,
				MultipleMax:    0,
				LikertVariants: nil,
				LMin:           0,
				LMax:           0,
				RMin:           0,
				RMax:           0,
				LText:          "",
				RText:          "",
				MText:          "",
				Lists:          nil,
				Trigger:        "",
				Target:         "",
				Show:           0,
				AttributeJson:  "",
			}
			q.AddVariant("1", "Среднее", "1")
			q.AddVariant("2", "Среднее специальное", "2")
			q.AddVariant("3", "Незаконченное высшее", "3")
			q.AddVariant("4", "Высшее", "4")
			q.AddVariant("5", "Второе высшее/MBA/аспирантура", "5")
			block.Screen().AddQuestion(q)

			q = entities.Question{
				ID:             9,
				Code:           "degree",
				Type:           "radio",
				SubType:        "",
				Shuffle:        false,
				Bank:           "",
				Hardness:       0,
				TimeLimit:      0,
				Text:           "Наличие ученой степени",
				Media:          "",
				Variants:       []entities.Variant{},
				AcceptEmpty:    false,
				MinVal:         0,
				MaxVal:         0,
				Step:           0,
				Placeholder:    "",
				DefaultValue:   "",
				ValidateStr:    "",
				SelectImage:    false,
				Multiple:       false,
				MultipleMin:    0,
				MultipleMax:    0,
				LikertVariants: nil,
				LMin:           0,
				LMax:           0,
				RMin:           0,
				RMax:           0,
				LText:          "",
				RText:          "",
				MText:          "",
				Lists:          nil,
				Trigger:        "",
				Target:         "",
				Show:           0,
				AttributeJson:  "",
			}
			q.AddVariant("1", "Да", "1")
			q.AddVariant("2", "Нет", "2")
			block.Screen().AddQuestion(q)

			q = entities.Question{
				ID:             10,
				Code:           "experience_pharma",
				Type:           "radio",
				SubType:        "",
				Shuffle:        false,
				Bank:           "",
				Hardness:       0,
				TimeLimit:      0,
				Text:           "Ваш стаж работы на фармацевтическом рынке, лет",
				Media:          "",
				Variants:       []entities.Variant{},
				AcceptEmpty:    false,
				MinVal:         0,
				MaxVal:         0,
				Step:           0,
				Placeholder:    "",
				DefaultValue:   "",
				ValidateStr:    "",
				SelectImage:    false,
				Multiple:       false,
				MultipleMin:    0,
				MultipleMax:    0,
				LikertVariants: nil,
				LMin:           0,
				LMax:           0,
				RMin:           0,
				RMax:           0,
				LText:          "",
				RText:          "",
				MText:          "",
				Lists:          nil,
				Trigger:        "",
				Target:         "",
				Show:           0,
				AttributeJson:  "",
			}
			q.AddVariant("1", "Меньше года", "1")
			q.AddVariant("2", "1-3 года", "2")
			q.AddVariant("3", "4-6 лет", "3")
			q.AddVariant("4", "7-10 лет", "4")
			q.AddVariant("5", "Более 10 лет", "5")
			block.Screen().AddQuestion(q)

			q = entities.Question{
				ID:             11,
				Code:           "experience_company",
				Type:           "radio",
				SubType:        "",
				Shuffle:        false,
				Bank:           "",
				Hardness:       0,
				TimeLimit:      0,
				Text:           "Ваш стаж на текущем месте работы, лет",
				Media:          "",
				Variants:       []entities.Variant{},
				AcceptEmpty:    false,
				MinVal:         0,
				MaxVal:         0,
				Step:           0,
				Placeholder:    "",
				DefaultValue:   "",
				ValidateStr:    "",
				SelectImage:    false,
				Multiple:       false,
				MultipleMin:    0,
				MultipleMax:    0,
				LikertVariants: nil,
				LMin:           0,
				LMax:           0,
				RMin:           0,
				RMax:           0,
				LText:          "",
				RText:          "",
				MText:          "",
				Lists:          nil,
				Trigger:        "",
				Target:         "",
				Show:           0,
				AttributeJson:  "",
			}
			q.AddVariant("1", "Меньше года", "1")
			q.AddVariant("2", "1-3 года", "2")
			q.AddVariant("3", "4-6 лет", "3")
			q.AddVariant("4", "7-10 лет", "4")
			q.AddVariant("5", "Более 10 лет", "5")
			block.Screen().AddQuestion(q)

			q = entities.Question{
				ID:             12,
				Code:           "position",
				Type:           "radio",
				SubType:        "",
				Shuffle:        false,
				Bank:           "",
				Hardness:       0,
				TimeLimit:      0,
				Text:           "Ваша позиция",
				Media:          "",
				Variants:       []entities.Variant{},
				AcceptEmpty:    false,
				MinVal:         0,
				MaxVal:         0,
				Step:           0,
				Placeholder:    "",
				DefaultValue:   "",
				ValidateStr:    "",
				SelectImage:    false,
				Multiple:       false,
				MultipleMin:    0,
				MultipleMax:    0,
				LikertVariants: nil,
				LMin:           0,
				LMax:           0,
				RMin:           0,
				RMax:           0,
				LText:          "",
				RText:          "",
				MText:          "",
				Lists:          nil,
				Trigger:        "",
				Target:         "",
				Show:           0,
				AttributeJson:  "",
			}
			q.AddVariant("1", "Специалист", "1")
			q.AddVariant("2", "Менеджер", "2")
			q.AddVariant("3", "Топ-менеджер", "3")
			block.Screen().AddQuestion(q)

			q = entities.Question{
				ID:             13,
				Code:           "region",
				Type:           "radio",
				SubType:        "",
				Shuffle:        false,
				Bank:           "",
				Hardness:       0,
				TimeLimit:      0,
				Text:           "Ваш регион",
				Media:          "",
				Variants:       []entities.Variant{},
				AcceptEmpty:    false,
				MinVal:         0,
				MaxVal:         0,
				Step:           0,
				Placeholder:    "",
				DefaultValue:   "",
				ValidateStr:    "",
				SelectImage:    false,
				Multiple:       false,
				MultipleMin:    0,
				MultipleMax:    0,
				LikertVariants: nil,
				LMin:           0,
				LMax:           0,
				RMin:           0,
				RMax:           0,
				LText:          "",
				RText:          "",
				MText:          "",
				Lists:          nil,
				Trigger:        "",
				Target:         "",
				Show:           0,
				AttributeJson:  "",
			}
			q.AddVariant("1", "Москва и МО", "1")
			q.AddVariant("2", "Центральный федеральный округ", "2")
			q.AddVariant("3", "Южный федеральный округ", "3")
			q.AddVariant("4", "Северо-Западный федеральный округ", "4")
			q.AddVariant("5", "Дальневосточный федеральный округ", "5")
			q.AddVariant("6", "Сибирский федеральный округ", "6")
			q.AddVariant("7", "Уральский федеральный округ", "7")
			q.AddVariant("8", "Приволжский федеральный округ", "8")
			q.AddVariant("9", "Северо-Кавказский федеральный округ", "9")
			q.AddVariant("10", "Крымский федеральный округ", "10")
			block.Screen().AddQuestion(q)

			q = entities.Question{
				ID:             14,
				Code:           "department",
				Type:           "radio",
				SubType:        "",
				Shuffle:        false,
				Bank:           "",
				Hardness:       0,
				TimeLimit:      0,
				Text:           "Направление Вашей работы",
				Media:          "",
				Variants:       []entities.Variant{},
				AcceptEmpty:    false,
				MinVal:         0,
				MaxVal:         0,
				Step:           0,
				Placeholder:    "",
				DefaultValue:   "",
				ValidateStr:    "",
				SelectImage:    false,
				Multiple:       false,
				MultipleMin:    0,
				MultipleMax:    0,
				LikertVariants: nil,
				LMin:           0,
				LMax:           0,
				RMin:           0,
				RMax:           0,
				LText:          "",
				RText:          "",
				MText:          "",
				Lists:          nil,
				Trigger:        "",
				Target:         "",
				Show:           0,
				AttributeJson:  "",
			}
			q.AddVariant("1", "Продажи", "1")
			q.AddVariant("2", "Маркетинг", "2")
			q.AddVariant("3", "Медицинский отдел (сотрудники, обеспечивающие продвижение продуктов среди врачей-исследователей и др.членов научного сообщества)", "3")
			q.AddVariant("4", "Исследования и разработки (сотрудники, занимающиеся разработкой и совершенствованием лекарственных средств, а также проводящие клинические исследования)", "4")
			q.AddVariant("5", "Поддержка (ИТ, HR, бухгалтерия и т.д.) (все службы поддержки бизнеса – бизнес-офис, бэк-офис, непрофильные для фармрынка)", "5")
			q.AddVariant("6", "Производство", "6")
			q.AddVariant("7", "Другие (графа для сотрудников, не относящих себя ни к одной из категорий выше)", "7")
			block.Screen().AddQuestion(q)
		})

		var unknown core.Block
		state.Block("unknown", &unknown).Before(func(block core.BeforeBlock) {

			companyMode := block.GetStorageGlobal().GetString("company_mode")
			var bCompanies []string

			for id := range companiesSlice {
				if strconv.Itoa(id) != companyMode || companyMode == "" {
					bCompanies = append(bCompanies, strconv.Itoa(id))
				}
			}

			block.GetStorageGlobal().SetStringSlice("companiesListId", bCompanies)
		}).Main(func(block core.MainBlock) {
			block.Screen().SetSubmitText("Далее")
			q := entities.Question{
				ID:             15,
				Code:           "awareness",
				Type:           "checkbox",
				SubType:        "media",
				Shuffle:        false,
				Bank:           "",
				Hardness:       0,
				TimeLimit:      0,
				Text:           "Ниже расположены логотипы компаний в фармацевтической сфере. <br/>Какие из этих компаний Вы НЕ знаете как работодателей?<br/>Отметьте КАЖДУЮ ИЗ НЕЗНАКОМЫХ Вам компаний, нажимая на логотип (он станет ярче)",
				Media:          "",
				Variants:       []entities.Variant{},
				AcceptEmpty:    true,
				MinVal:         0,
				MaxVal:         0,
				Step:           0,
				Placeholder:    "",
				DefaultValue:   "",
				ValidateStr:    "",
				SelectImage:    false,
				Multiple:       false,
				MultipleMin:    0,
				MultipleMax:    0,
				LikertVariants: nil,
				LMin:           0,
				LMax:           0,
				RMin:           0,
				RMax:           0,
				LText:          "",
				RText:          "",
				MText:          "",
				Lists:          nil,
				Trigger:        "",
				Target:         "",
				Show:           0,
				AttributeJson:  "",
			}

			for i, companyID := range block.StorageGlobal().GetStringSlice("companiesListId") {
				companyIDInt, err := strconv.Atoi(companyID)
				if err == nil && len(companiesSlice) > companyIDInt {
					q.AddMediaVariant(companyID, companiesSlice[companyIDInt], strconv.Itoa(i), "/habr-farma/id"+companyID+".png")
				}
			}

			//тут заменить на название папки
			block.Screen().AddQuestion(q)
		})

		var positive core.Block
		state.Block("positive", &positive).Before(func(block core.BeforeBlock) {

		}).Main(func(block core.MainBlock) {
			block.Screen().SetSubmitText("Далее")
			q := entities.Question{
				ID:             16,
				Code:           "positive",
				Type:           "checkbox",
				SubType:        "media",
				Shuffle:        false,
				Bank:           "",
				Hardness:       0,
				TimeLimit:      0,
				Text:           "Ниже расположены логотипы компаний-работодателей в аптечной сфере. <br/>Кого бы Вы порекомендовали своим знакомым и друзьям как ЛУЧШЕГО работодателя? <br/><br/>Выберите 2-3 компании, нажимая на их логотипы. <br/> Если Вы не видите логотип нужной компании, нажмите на значок вопросительного знака и впишите название компании.",
				Media:          "",
				Variants:       []entities.Variant{},
				AcceptEmpty:    false,
				MinVal:         0,
				MaxVal:         0,
				Step:           0,
				Placeholder:    "",
				DefaultValue:   "",
				ValidateStr:    "",
				SelectImage:    false,
				Multiple:       false,
				MultipleMin:    2,
				MultipleMax:    3,
				LikertVariants: nil,
				LMin:           0,
				LMax:           0,
				RMin:           0,
				RMax:           0,
				LText:          "",
				RText:          "",
				MText:          "",
				Lists:          nil,
				Trigger:        "",
				Target:         "",
				Show:           0,
				AttributeJson:  "",
			}

			unknownCompanies := block.AnswersGlobal().GetAnswersByBlockName("unknown")
			unknownCompaniesMap := map[string]string{}
			for _, answer := range unknownCompanies.Answers {
				if answer.Question.Code == "awareness" {
					for _, unknownCompany := range answer.Values {
						unknownCompaniesMap[unknownCompany] = unknownCompany
					}
				}
			}

			for i, company := range companiesSlice {
				_, unknown := unknownCompaniesMap[strconv.Itoa(i)]
				if !unknown {
					q.AddMediaVariant(strconv.Itoa(i), company, strconv.Itoa(i), "/habr-farma/id"+strconv.Itoa(i)+".png")
				}
				//тут заменить на название папки
			}

			q.AddMediaVariant("other", "Другое", "9999", "/habr-farma/other.png")
			//тут заменить на номер
			triggerID := block.Screen().AddQuestionWithAjax(q)
			block.Screen().AddAjaxArea(triggerID, func(Screen func() core.DynamicScreen) {
				q := entities.Question{
					ID:             201,
					Code:           "positive_other",
					Type:           "text",
					SubType:        "",
					Shuffle:        false,
					Bank:           "",
					Hardness:       0,
					TimeLimit:      0,
					Text:           "",
					Media:          "",
					Variants:       nil,
					AcceptEmpty:    false,
					MinVal:         0,
					MaxVal:         0,
					Step:           0,
					Placeholder:    "",
					DefaultValue:   "",
					ValidateStr:    "",
					SelectImage:    false,
					Multiple:       false,
					MultipleMin:    0,
					MultipleMax:    0,
					LikertVariants: nil,
					LMin:           0,
					LMax:           0,
					RMin:           0,
					RMax:           0,
					LText:          "",
					RText:          "",
					MText:          "",
					Lists:          nil,
					Trigger:        "",
					Target:         "",
					Show:           0,
					AttributeJson:  "",
				}
				for _, value := range Screen().GetAjaxValues() {
					if value == "other" {
						Screen().AddQuestion(q)
						break
					}
				}
			})
		})

		var negative core.Block
		state.Block("negative", &negative).Before(func(block core.BeforeBlock) {

		}).Main(func(block core.MainBlock) {
			block.Screen().SetSubmitText("Далее")
			q := entities.Question{
				ID:             17,
				Code:           "negative",
				Type:           "checkbox",
				SubType:        "media",
				Shuffle:        false,
				Bank:           "",
				Hardness:       0,
				TimeLimit:      0,
				Text:           "Ниже расположены логотипы компаний-работодателей в фармацевтической сфере. <br/>Кого бы Вы назвали ХУДШИМ работодателем, и не стали бы рекомендовать?<br/><br/>Выберите 2-3 компании, нажимая на их логотипы. <br/> Если Вы не видите логотип нужной компании, нажмите на значок вопросительного знака и впишите название компании.",
				Media:          "",
				Variants:       []entities.Variant{},
				AcceptEmpty:    false,
				MinVal:         0,
				MaxVal:         0,
				Step:           0,
				Placeholder:    "",
				DefaultValue:   "",
				ValidateStr:    "",
				SelectImage:    false,
				Multiple:       false,
				MultipleMin:    2,
				MultipleMax:    3,
				LikertVariants: nil,
				LMin:           0,
				LMax:           0,
				RMin:           0,
				RMax:           0,
				LText:          "",
				RText:          "",
				MText:          "",
				Lists:          nil,
				Trigger:        "",
				Target:         "",
				Show:           0,
				AttributeJson:  "",
			}

			unknownCompanies := block.AnswersGlobal().GetAnswersByBlockName("unknown")
			unknownCompaniesMap := map[string]string{}
			for _, answer := range unknownCompanies.Answers {
				if answer.Question.Code == "awareness" {
					for _, unknownCompany := range answer.Values {
						unknownCompaniesMap[unknownCompany] = unknownCompany
					}
				}
			}

			positiveCompanies := block.AnswersGlobal().GetAnswersByBlockName("positive")
			positiveCompaniesMap := map[string]string{}
			for _, answer := range positiveCompanies.Answers {
				if answer.Question.Code == "positive" {
					for _, positiveCompany := range answer.Values {
						positiveCompaniesMap[positiveCompany] = positiveCompany
					}
				}
			}

			for i, company := range companiesSlice {
				_, unknown := unknownCompaniesMap[strconv.Itoa(i)]
				if !unknown {
					_, positive := positiveCompaniesMap[strconv.Itoa(i)]
					if !positive {
						q.AddMediaVariant(strconv.Itoa(i), company, strconv.Itoa(i), "/habr-farma/id"+strconv.Itoa(i)+".png")
					}
				}
				//лого файл
			}
			q.AddMediaVariant("other", "Другое", "9999", "/habr-farma/other.png")
			//номер
			triggerID := block.Screen().AddQuestionWithAjax(q)
			block.Screen().AddAjaxArea(triggerID, func(Screen func() core.DynamicScreen) {
				q := entities.Question{
					ID:             202,
					Code:           "negative_other",
					Type:           "text",
					SubType:        "",
					Shuffle:        false,
					Bank:           "",
					Hardness:       0,
					TimeLimit:      0,
					Text:           "",
					Media:          "",
					Variants:       nil,
					AcceptEmpty:    false,
					MinVal:         0,
					MaxVal:         0,
					Step:           0,
					Placeholder:    "",
					DefaultValue:   "",
					ValidateStr:    "",
					SelectImage:    false,
					Multiple:       false,
					MultipleMin:    0,
					MultipleMax:    0,
					LikertVariants: nil,
					LMin:           0,
					LMax:           0,
					RMin:           0,
					RMax:           0,
					LText:          "",
					RText:          "",
					MText:          "",
					Lists:          nil,
					Trigger:        "",
					Target:         "",
					Show:           0,
					AttributeJson:  "",
				}
				for _, value := range Screen().GetAjaxValues() {
					if value == "other" {
						Screen().AddQuestion(q)
						break
					}
				}
			})
		})

		var indicators core.Block
		state.Block("indicators", &indicators).Before(func(block core.BeforeBlock) {

			var positiveCompanies []string

			answerPositiveCompanies := block.GetAnswersGlobal().GetAnswersByBlockName("positive")
			for _, answer := range answerPositiveCompanies.Answers {
				if answer.Question.Code == "positive" {
					for _, company := range answer.Values {

						if company == "other" {

							positiveOthers := block.GetAnswersGlobal().GetAnswersByBlockName("positive")

							find := false
							for _, positiveOther := range positiveOthers.Answers {
								if positiveOther.Question.Code == "positive_other" {
									find = true
									positiveCompanies = append(positiveCompanies, positiveOther.Values[0])
									break
								}
							}
							if find {
								continue
							}
						}

						if company != "0" {
							positiveCompanies = append(positiveCompanies, company)
						}
					}
				}
			}

			var negativeCompanies []string
			answerNegativeCompanies := block.GetAnswersGlobal().GetAnswersByBlockName("negative")
			for _, answer := range answerNegativeCompanies.Answers {
				if answer.Question.Code == "negative" {
					for _, company := range answer.Values {

						if company == "other" {

							negativeOthers := block.GetAnswersGlobal().GetAnswersByBlockName("negative")

							find := false
							for _, negativeOther := range negativeOthers.Answers {
								if negativeOther.Question.Code == "negative_other" {
									find = true
									negativeCompanies = append(negativeCompanies, negativeOther.Values[0])
									break
								}
							}
							if find {
								continue
							}
						}

						negativeCompanies = append(negativeCompanies, company)
					}
				}
			}

			var companies []string
			companies = append(companies, positiveCompanies...)
			companies = append(companies, negativeCompanies...)
			companyMode := block.GetStorageGlobal().GetString("company_mode")

			if companyMode != "" {
				find := false
				for _, id := range companies {
					if companyMode == id {
						find = true
					}
				}
				if !find {
					companies = append([]string{companyMode}, companies...)
				}
			}

			var finalCompanies []string
			for _, company := range companies {
				finalCompanies = append(finalCompanies, company)
				finalCompanies = append(finalCompanies, company)
			}

			block.GetStorage().SetStringSlice("companies", finalCompanies)
			block.SetIterationsCount(len(finalCompanies))

		}).Main(func(block core.MainBlock) {
			block.Screen().SetSubmitText("Далее")

			companies := block.Storage().GetStringSlice("companies")
			currentScreen := block.IterationCurrent()
			currentCompany := currentScreen

			companiesMap := map[string]string{}
			for i, company := range companiesSlice {
				companiesMap[strconv.Itoa(i)] = company
			}

			companyID := companies[currentCompany]
			count := 2
			if (currentCompany % 2) == 0 {
				count = 1
			}
			block.Screen().AddHeader("Экран  " + strconv.Itoa(count) + " / 2")
			if currentCompany >= len(companies) {
				block.SetSkip(true)
				return
			}

			block.Screen().AddString("Охарактеризуйте компанию.<br/> ")

			_, exist := companiesMap[companyID]
			if exist {
				block.Screen().AddImage("/habr-farma/id"+companyID+".png", "")
				//файл
			} else {
				block.Screen().AddString(companyID + "<br/>")
			}

			block.Screen().AddString(" Какие характеристики ей присущи, на ваш взгляд? <br/><br/>Перенесите направо НАИБОЛЕЕ свойственные характеристики компании, налево НАИМЕНЕЕ свойственные характеристики. Выберите от 3 до 5 характеристик в каждом случае.")
			if (currentScreen % 2) == 0 {
				q := entities.Question{
					ID:             21,
					Code:           "idk",
					Type:           "checkbox",
					SubType:        "",
					Shuffle:        false,
					Bank:           "",
					Hardness:       0,
					TimeLimit:      0,
					Text:           "",
					Media:          "",
					Variants:       []entities.Variant{},
					AcceptEmpty:    true,
					MinVal:         0,
					MaxVal:         0,
					Step:           0,
					Placeholder:    "",
					DefaultValue:   "",
					ValidateStr:    "",
					SelectImage:    false,
					Multiple:       false,
					MultipleMin:    0,
					MultipleMax:    0,
					LikertVariants: nil,
					LMin:           0,
					LMax:           0,
					RMin:           0,
					RMax:           0,
					LText:          "",
					RText:          "",
					MText:          "",
					Lists:          nil,
					Trigger:        "",
					Target:         "company_id",
					TargetID:       companyID,
					Show:           0,
					AttributeJson:  "",
				}
				q.AddVariant("1", "Я не могу оценить эту компанию", "1")
				q.Trigger = strconv.Itoa(q.ID) + q.Code + q.Target + q.TargetID + q.Type
				block.Screen().AddAjaxArea(q.Trigger, func(Screen func() core.DynamicScreen) {

					show := true
					if len(Screen().GetAjaxValues()) > 0 {
						for _, value := range Screen().GetAjaxValues() {
							if value == "1" {
								show = false
							}
						}
					}

					if show {
						q := entities.Question{
							ID:             18,
							Code:           "im_worker",
							Type:           "checkbox",
							SubType:        "",
							Shuffle:        false,
							Bank:           "",
							Hardness:       0,
							TimeLimit:      0,
							Text:           "Нажмите, если работали в этой компании",
							Media:          "",
							Variants:       []entities.Variant{},
							AcceptEmpty:    true,
							MinVal:         0,
							MaxVal:         0,
							Step:           0,
							Placeholder:    "",
							DefaultValue:   "",
							ValidateStr:    "",
							SelectImage:    false,
							Multiple:       false,
							MultipleMin:    0,
							MultipleMax:    0,
							LikertVariants: nil,
							LMin:           0,
							LMax:           0,
							RMin:           0,
							RMax:           0,
							LText:          "",
							RText:          "",
							MText:          "",
							Lists:          nil,
							Trigger:        "",
							Target:         companyID,
							Show:           0,
							AttributeJson:  "",
						}
						q.AddVariant("1", "Я работал в этой компании", "1")
						Screen().AddQuestion(q)
						// TODO Screen().AddString("Какие характеристики ей присущи, на ваш взгляд? <br/><br/>Перенесите направо НАИБОЛЕЕ свойственные характеристики компании, налево НАИМЕНЕЕ свойственные характеристики. Выберите от 3 до 5 характеристик в каждом случае.")
						q = entities.Question{
							ID:             20,
							Code:           "indicator_2",
							Type:           "leftright",
							SubType:        "",
							Shuffle:        true,
							Bank:           "",
							Hardness:       0,
							TimeLimit:      0,
							Text:           "",
							Media:          "",
							Variants:       []entities.Variant{},
							AcceptEmpty:    false,
							MinVal:         0,
							MaxVal:         0,
							Step:           0,
							Placeholder:    "",
							DefaultValue:   "",
							ValidateStr:    "",
							SelectImage:    false,
							Multiple:       false,
							MultipleMin:    0,
							MultipleMax:    0,
							LikertVariants: nil,
							LMin:           3,
							LMax:           5,
							RMin:           3,
							RMax:           5,
							LText:          "Наименее свойственные",
							RText:          "Наиболее свойственные",
							MText:          "",
							Lists:          nil,
							Trigger:        "",
							Target:         companyID,
							Show:           0,
							AttributeJson:  "",
							ErrorText:      "Выберите от 3 до 5 характеристик в каждую сторону.",
						}
						q.AddVariant("20", "Зависимость дохода от собственной эффективности", "20")
						q.AddVariant("21", "Наличие гибкого графика", "21")
						q.AddVariant("22", "Возможность удаленной работы", "22")
						q.AddVariant("23", "Удобные инструменты коммуникаций в Компании", "23")
						q.AddVariant("24", "Комфортность офиса", "24")
						q.AddVariant("25", "Близость работы от дома", "25")
						q.AddVariant("26", "Интересная локация офиса", "26")
						q.AddVariant("27", "Профессионализм сотрудников", "27")
						q.AddVariant("28", "Уважительное отношение руководителей", "28")
						q.AddVariant("29", "Самостоятельность в принятии решений", "29")
						q.AddVariant("30", "Информационная открытость", "30")
						q.AddVariant("31", "Открытость руководства к мнению сотрудников", "31")
						q.AddVariant("32", "Хорошие отношения в коллективе", "32")
						q.AddVariant("33", "Баланс между работой и личной жизнью", "33")
						q.AddVariant("34", "Забота о физическом и ментальном здоровье сотрудников", "34")
						q.AddVariant("35", "Творческая атмосфера", "35")
						q.AddVariant("36", "Культура многообразия (Diversity & Inclusion)", "36")
						q.AddVariant("37", "Интересные корпоративные мероприятия", "37")
						q.AddVariant("38", "Современные подходы к управлению проектами", "38")
						Screen().AddQuestion(q)
					}
				})
				block.Screen().AddQuestion(q)
			} else {

				ans := block.AnswersGlobal().GetAnswerByQuestionCodeAndIDAndTargetAndTargetID("idk", 21, "company_id", companyID)
				if ans != nil && len(ans.Values) > 0 {
					for _, val := range ans.Values {
						if val == "1" {
							block.SetSkip(true)
							return
						}
					}
				}

				q := entities.Question{
					ID:             19,
					Code:           "indicator_2",
					Type:           "leftright",
					SubType:        "",
					Shuffle:        true,
					Bank:           "",
					Hardness:       0,
					TimeLimit:      0,
					Text:           "",
					Media:          "",
					Variants:       []entities.Variant{},
					AcceptEmpty:    false,
					MinVal:         0,
					MaxVal:         0,
					Step:           0,
					Placeholder:    "",
					DefaultValue:   "",
					ValidateStr:    "",
					SelectImage:    false,
					Multiple:       false,
					MultipleMin:    0,
					MultipleMax:    0,
					LikertVariants: nil,
					LMin:           3,
					LMax:           5,
					RMin:           3,
					RMax:           5,
					LText:          "Наименее свойственные",
					RText:          "Наиболее свойственные",
					MText:          "",
					Lists:          nil,
					Trigger:        "",
					Target:         companyID,
					Show:           0,
					AttributeJson:  "",
					ErrorText:      "Выберите от 3 до 5 характеристик в каждую сторону.",
				}
				q.AddVariant("1", "Известность компании", "1")
				q.AddVariant("2", "Стабильность компании", "2")
				q.AddVariant("3", "Темпы роста компании", "3")
				q.AddVariant("4", "Международный/транснациональный опыт", "4")
				q.AddVariant("5", "Инновационность и современность проектов компании", "5")
				q.AddVariant("6", "Качество/ уникальность продуктов и услуг", "6")
				q.AddVariant("7", "Этичное ведение бизнеса", "7")
				q.AddVariant("8", "Социальная ответственность и участие в благотворительности", "8")
				q.AddVariant("9", "Вклад компании в развитие общества", "9")
				q.AddVariant("10", "Возможности профессионального развития", "10")
				q.AddVariant("11", "Интересное содержание работы", "11")
				q.AddVariant("12", "Достаточный объём полномочий и ответственности", "12")
				q.AddVariant("13", "Скорость построения карьеры", "13")
				q.AddVariant("14", "Международные стажировки и обучение", "14")
				q.AddVariant("15", "Ценность работы в компании для резюме", "15")
				q.AddVariant("16", "Масштаб и амбициозность проектов и задач компании", "16")
				q.AddVariant("17", "Участие Компании в государственных проектах", "17")
				q.AddVariant("18", "Уровень заработной платы", "18")
				q.AddVariant("19", "Соцпакет (оплата питания, страховка и др.)", "19")
				block.Screen().AddQuestion(q)
			}
		})

		state.EndBlock().Main(func(b core.MainBlock) {
			b.Screen().AddString("Спасибо за участие в опросе")
		})
	}
	core.Run()
}
